

# Cam's pretty-printing of titles
preexec() {
    local TITLE=$1
    print -Pn "\e]0;$TITLE\a"
}
precmd() {
    local TITLE=$PWD
    print -Pn "\e]0;$TITLE\a"
}

autoload -U colors && colors

PS1="%{$fg[blue]%}%n%{$reset_color%}@%{$fg[red]%}%m%{$reset_color%}:%{$fg[black]%}%~%{$reset_color%}$ "

[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc"
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

fpath+=~/.config/zsh/zfunc

autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit

# Glob dotfiles
_comp_options+=(globdots)

HISTFILE=~/.config/zsh/histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory notify

#bindkey -v
#bindkey -M menuselect 'h' vi-backward-char
#bindkey -M menuselect 'j' vi-down-line-or-history
#bindkey -M menuselect 'k' vi-up-line-or-history
#bindkey -M menuselect 'l' vi-forward-char
#bindkey -v '^?' backward-delete-char

source "$HOME/dev/zsh-vi-mode/zsh-vi-mode.plugin.zsh"


function open {
    xdg-open "$1" &>/dev/null & disown
}

function lfcd {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        if [ -d "$dir" ]; then
            if [ "$dir" != "$(pwd)" ]; then
                cd "$dir"
            fi
        fi
    fi
}


# Stolen/adapted from:
# https://github.com/drduh/YubiKey-Guide#using-keys
function cc-encrypt {
    output="${1}.enc"
    echo "$output"
    gpg --encrypt --armor --output "${output}" -r "0xCB8916955CD34B50" "${1}" && echo "${1} -> ${output}"
}

function cc-decrypt {
    output=$(echo "${1}" | sed -e 's/\.enc$//')

    gpg --decrypt --output "${output}" "${1}" && echo "${1} -> ${output}"
}

bindkey -s '^o' 'lfcd\n' # browser

