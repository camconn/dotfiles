#!/bin/sh
# Copy my dotfiles to this directory.

cp ~/.emacs.d/init.el init.el
cp ~/.config/zsh/.zshrc zshrc
cp ~/.profile profile
