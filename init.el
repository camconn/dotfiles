;; -*- coding: utf-8 -*-
;; init.el - Cam's Emacs Configuration File
;;
;; It sets up evil-mode and some other things that may be useful for former
;; (neo-)vim users.
 ;;
;; © 2020 Cameron Conn

(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  )
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-backends
   '(company-capf company-bbdb company-semantic company-cmake company-files
                  (company-dabbrev-code company-gtags company-etags company-keywords)
                  company-oddmuse company-dabbrev))
 '(custom-safe-themes
   '("fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" "bb08c73af94ee74453c90422485b29e5643b73b05e8de029a6909af6a3fb3f58" default))
 '(evil-want-C-d-scroll t)
 '(package-selected-packages
   '(go-mode rustic dap-mode rust-mode haxe-mode origami all-the-icons-dired all-the-icons projectile lsp-ui dired-sidebar org-view-mode org-modern ## magit lsp-mode auto-complete flycheck zig-mode use-package evil-org company markdown-mode cmake-mode spacemacs-theme evil-collection adoc-mode evil)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; ======================================== EVIL mode ========================================
(setq evil-want-keybinding nil
      evil-want-integration t)

(defvar cam-leader-map (make-sparse-keymap)
  "Keymap for \"leader key\" shortcuts.")

(use-package evil
  :ensure t
  :config
  (defun cam-setup-evil ()
    (evil-mode 1)

    ; make TAB act like vim
    (define-key evil-insert-state-map (kbd "TAB") 'tab-to-tab-stop)

    ; Setup next buffer et al with Vim leader keys
    (define-key evil-normal-state-map "," cam-leader-map)
    (define-key cam-leader-map "n" 'next-buffer)
    (define-key cam-leader-map "b" 'previous-buffer)
    (define-key cam-leader-map "x" 'kill-current-buffer)
    (define-key cam-leader-map "s" 'dired-sidebar-toggle-sidebar))
  (cam-setup-evil))

;; ============================== Platform-Specific Settings ==================================
(if (eq system-type 'windows-nt)
    '((setq exec-path (append exec-path
                            ;'("C:/Users/Cameron Conn/Documents/code/zls/zig-cache/bin")))
                            '("C:/Users/cameron.conn/src/zls/zig-cache/bin")))
      
      ; Get rid of annoying issues with *Nix-vs-Windows coding
      (prefer-coding-system 'utf-8-unix)
      )
  '((setq exec-path (append exec-path
                            '("/home/cam/.local/cargo/bin/rust-analyzer")))))

(prefer-coding-system 'utf-8-unix)

;; ============================= Filetype & Major Mode Settings ===============================

; AsciiDoc stuff
(autoload 'adoc-mode "adoc-mode" nil t)
(add-to-list 'auto-mode-alist (cons "\\.adoc\\'" 'adoc-mode))

; Flex-mode
(use-package flex
  :mode ("\\.l\\'" . flex-mode)
  :load-path (lambda () "~/.local/emacs/flex")
  )

; CMake stuff
;(add-to-list 'auto-mode-alist '("CMakeLists\\.txt\\'" . cmake-mode))
;(add-to-list 'auto-mode-alist '("\\.cmake\\'" . cmake-mode))
(use-package cmake-mode :mode "CMakeLists.txt\\'")

; Company Mode
(add-hook 'after-init-hook 'global-company-mode)

; GDB stuff


; Tabs
(defun setup-lean-tabs()
  (setq tab-width 2)
  (c-set-offset 'arglist-intro '+))
(add-hook 'c-mode-hook      'setup-lean-tabs)
(add-hook 'c++-mode-hook    'setup-lean-tabs)
(add-hook 'ps-mode-hook     'setup-lean-tabs)
(add-hook 'asm-mode-hook    'setup-lean-tabs)
(add-hook 'elisp-mode-hook  'setup-lean-tabs)

(defun setup-big-tabs()
  (setq tab-width 4))
(add-hook 'adoc-mode-hook       'setup-big-tabs)
(add-hook 'flex-mode-hook       'setup-big-tabs)
(add-hook 'python-mode-hook     'setup-big-tabs)


; LaTeX/TeX/Plaintext modes
; Set fill width at 80 chars
(defun cam-text-stuff()
  ;(flyspell-mode)
  (set-fill-column 80)
  (turn-on-auto-fill))

(add-hook 'adoc-mode-hook 'cam-text-stuff)
(add-hook 'tex-mode-hook 'cam-text-stuff)
(add-hook 'latex-mode-hook 'cam-text-stuff)
(add-hook 'text-mode-hook 'cam-text-stuff)

; Use utf-8 by default
(setq-default buffer-file-coding-system 'utf-8-unix)

(use-package projectile)

(defun cam-code-compile()
  "Compile the code with LSP"
  (interactive)
    (cond ((eq major-mode 'zig-mode)    (call-interactively 'zig-compile))
          ((eq major-mode 'rustic-mode) (call-interactively 'rustic-compile))
	  ; If not in any of the above modes, just do nothing
          (print "Not enabled in this mode")))

(use-package lsp-mode
  :hook ((js-mode . lsp)
         (javascript-mode . lsp)
         (typescript-mode . lsp)
         (zig-mode . lsp)
         (haxe-mode . lsp)
         (rust-mode . lsp))
  :init
    
    (flycheck-mode)
    
    ;(lsp-register-client
    ; (make-lsp-client
    ;  :new-connection (lsp-stdio-connection "zls")
    ;  :major-modes '(zig-mode)
    ;  :server-id 'zls))
    ;(add-to-list 'lsp-language-id-configuration '(zig-mode . "zig"))
    (setq lsp-zig-zls-executable "/home/cam/.local/bin/zls-0.9.0/zls");
    
    (setq lsp-signature-render-documentation t)
    (setq lsp-auto-guess-root t)
    (setq lsp-enable-indentation nil)

    ;(define-key lsp-mode-map (kbd "C-c h") 'eldoc-doc-buffer)
    ;(define-key lsp-mode-map (kbd "<f6>") 'lsp-find-references)
    (setq lsp-clients--haxe-executable "node")
    (setq lsp-clients--haxe-server-path (expand-file-name "~/dev/haxe-language-server/bin/server.js"))

    (setq lsp-rust-server 'rust-analyzer)
    (setq rustic-lsp-server 'rust-analyzer)
    (setq lsp-clients--rust-server-path (expand-file-name "~/.local/cargo/bin/rust-analyzer"))
    ;(lsp-rust-analyzer-cargo-watch-command "clippy")

    (define-key cam-leader-map "c" 'cam-code-compile)
        ; Change behavior based on what mode we are in

    (define-key cam-leader-map "d" 'lsp-find-definition)
    (define-key cam-leader-map "f" 'project-find-file)
    (define-key cam-leader-map "e" 'lsp-dired)
    (define-key cam-leader-map "m" 'lsp-rename)
    (define-key cam-leader-map "r" 'lsp-find-references))

(use-package lsp-ui
  :init
  (evil-collection-init 'lsp-ui-imenu))

(define-key cam-leader-map "t" 'dired-sidebar-toggle-sidebar)
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)

(use-package rustic)

;; ============================ Personal Preferences ==============================
; appearance
(load-theme 'spacemacs-light t)
;(set-frame-font "DejaVu Sans-Mono-12" nil t)
;(set-frame-font "DejaVu Sans Mono-12")
(set-face-attribute 'default nil :height 120 :family "DejaVu Sans Mono")
;(set-frame-font 'default)
;(set-default-font "DejaVu-Sans-Mono 12" nil t)
;(set-face-attribute 'default nil :height 120)

; disable menu and tool bars
(menu-bar-mode -1)
(tool-bar-mode -1)

; Line numbers
(global-display-line-numbers-mode)
(setq-default display-line-numbers-type 'relative)

; Tabs stuff
(setq-default tab-width 4
              indent-tabs-mode nil)
(setq make-backup-files nil)


; spellcheck and stuff
; unconditionally use hunspell
(cond
 ((executable-find "hunspell")
  (setq ispell-program-name "hunspell")
  (setq ispell-really-hunspell t)
  (setq ispell-local-dictionary "en_US")
  (setq ispell-local-dictionary-alist
               '(("en_US" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil nil nil utf-8)))))

;(flyspell-mode 1)

; Flyspell
;(add-hook 'prog-mode-hook 'flyspell-prog-mode)


; Stolen from https://www.emacswiki.org/emacs/EndOfLineTips
; We want Unix-like line endings only!
(defun no-junk-please-were-unixish ()
  (let ((coding-str (symbol-name buffer-file-coding-system)))
    (when (string-match "-\\(?:dos\\|mac\\)$" coding-str)
      (set-buffer-file-coding-system 'unix))))
(add-hook 'find-file-hook 'no-junk-please-were-unixish)


;; ==================================== EVIL collection ======================================
(evil-collection-init 'calc)
(evil-collection-init 'company)
(evil-collection-init 'custom)
(evil-collection-init 'doc-view)
(evil-collection-init 'debug)
(evil-collection-init 'dired)
(evil-collection-init 'dired-sidebar)
(evil-collection-init 'info)
(evil-collection-init 'image-dired)
(evil-collection-init 'magit)
(evil-collection-init 'package-menu)
;(evil-collection-init 'minibuffer)
(evil-collection-init 'xref)

; evil-org-mode
; github.com/Somelauw/evil-org-mode
(use-package evil-org
  :after org
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'evil-org-mode-hook
            (lambda ()
              (evil-org-set-key-theme)))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

; Folding support
;(use-package origami
;  :hook (prog-mode text-mode)
;
;  )

; Evil mode in package-list-packages
(evil-set-initial-state 'package-menu-mode 'motion)

(provide 'init)
;;; init.el ends here
