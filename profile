# Cam's profile settings
#
# © Cameron Conn 2016-2022


# Defaults
export EDITOR="nvim"
export TERMINAL="kitty"
export TERM="xterm"
export BROWSER="brave-bin"
export READER="mupdf"
export FILE="lf"
export LANG="en_US.UTF-8"
#export STATUSBAR="waybar"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.config/cache"

# $HOME Cleanup
export ZDOTDIR="$HOME/.config/zsh"
export INPUTRC="$HOME/.config/inputrc"
export LESSHISTFILE="-"
#export WGETRC="$HOME/.config/wget/wgetrc"
export PASSWORD_STORE_DIR="$HOME/.local/share/password-store"
export GTK2_RC_FILES="$HOME/.config/gtk-2.0/gtkrc-2.0"
export NOTMUCH_CONFIG="$HOME/.config/notmuch-config"

# Wayland stuff
#export XDG_RUNTIME_DIR=/tmp    # managed by systemd
export XKB_DEFAULT_LAYOUT=us
export XKB_DEFAULT_OPTIONS=caps:escape

# Use native wayland things
export CLUTTER_BACKEND=wayland
#export GDK_BACKEND=wayland     # gtk will use wayland by defaul't. dont set this
                                # or things will break
#export QT_QPA_PLATFORM=wayland
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
export SDL_VIDEODRIVER=wayland
export _JAVA_AWT_WM_NONREPARENTING=1
export MOZ_ENABLE_WAYLAND=1
#glfw requires glfw-wayland package

# Stuff for Golang
export GOROOT="/usr/lib/go"
export GOPATH="/home/cam/.local/go"
export PATH="$GOPATH/bin:$HOME/bin:$HOME/.local/bin:$PATH"

export PATH="$PATH:$HOME/.local/bin/zig-0.9.1"
export PATH="$PATH:$HOME/.local/bin/zls-0.9.0"
export PATH="$PATH:$HOME/.local/bin/julia-1.7.3/bin"

# Stuff for virtualenvwrapper and Python development
export WORKON_HOME="~/.config/envs"

# IPFS Repo
export IPFS_PATH="~/docs/ipfs"

# Rust stuff
#export PATH="$PATH:$HOME/.cargo/bin"
#export PATH="$HOME/.cargo/bin:$PATH"
export CARGO_HOME="$HOME/.local/cargo"
export RUSTUP_HOME="$HOME/.local/rustup"

# Dotnet Tools Setup
export PATH="$PATH:/home/cam/.dotnet/tools"

# PlatformIO Stuff
export PATH="$PATH:/home/cam/.platformio/penv/bin"


if [ -f /usr/bin/virtualenvwrapper.sh ]; then
    source /usr/bin/virtualenvwrapper.sh
fi

# SSH agent
if [ -z "$SSH_AUTH_SOCK" ] ; then
	eval `ssh-agent -s`
	#ssh-add
fi

# Autostart Sway
# inline version:
[ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ] && exec dbus-run-session sway
#if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
#	exec sway
#fi



. "/home/cam/.local/cargo/env"
